import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:flutter/foundation.dart';

import 'theme/colors.dart';
import 'top_bar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Curved Path',
      theme: appTheme,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ListWidget listWidget = ListWidget([
    "https://cdn.shopify.com/s/files/1/1061/1924/files/Smirk_Face_Emoji_Icon_60x60.png?14173495976923716614"
  ]);
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Container(
        child: Column(
          children: <Widget>[
            Container(color: Colors.red,height: 20,),
            getTop(),
            getTextField(),
            getFlatButton(context),
            Expanded(child: listWidget)
          ],
        ),
      ),
    );
  }

  FlatButton getFlatButton(BuildContext context) {
    return FlatButton.icon(
      onPressed: () {
        listWidget.addItem(controller.text);
        Toast.show(controller.text, context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      },
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30))),
      icon: Icon(Icons.add, color: Colors.white),
      label: Text(
        "ADD",
        style: TextStyle(color: Colors.white),
      ),
      color: Colors.red,
      disabledColor: Colors.red,
    );
  }

  Padding getTextField() {
    return Padding(
      padding: EdgeInsets.all(16),
      child: TextFormField(
        controller: controller,
        cursorColor: Colors.red,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.all_out),
          labelText: "Enter Emoticon URL",
          fillColor: Colors.white70,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(30))),
        ),
      ),
    );
  }
}

Builder getTop() {
  return Builder(builder: (context) {
    return Stack(
      children: <Widget>[
        TopBar(),
        getMenu(),
        Positioned(
          top: 85,
          left: MediaQuery.of(context).size.width * 0.32,
          child: ShadowText("Emoji Fetcher", size: 24.0),
        )
        /*Positioned(
                right: 42,
                top: 100,
                child: ShadowText("R4X H4X0R", size: 24.0),
              )*/
      ],
    );
  });
}

Container getMenu() {
  return Container(
    child: Stack(
      children: <Widget>[
        CustomPaint(
          painter: MenuCicleBackgroundPainter(radius: 110, circleColor: Colors.amber),
        ),
        Transform.rotate(
          angle: 70,
          child: Icon(
            Icons.menu,
            color: Colors.white,
            size: 90,
          ),
        ),
      ],
    ),
  );
}

class ShadowText extends StatelessWidget {
  final String text;
  final double size;

  ShadowText(this.text, {this.size = 16.0});

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: TextStyle(
            color: Colors.white,
            fontSize: size,
            shadows: [Shadow(offset: Offset(2, 2), blurRadius: 20)]));
  }
}

class ListWidget extends StatefulWidget {
  final List<String> list;
  final ListState listState = ListState();

  ListWidget(this.list);

  void addItem(String value) {
    listState.addItem(value);
  }

  @override
  State<StatefulWidget> createState() {
    return listState;
  }
}

class ListState extends State<ListWidget> {
  List<String> images = [];

  void addItem(String item) {
    setState(() {
      images.add(item);
    });
  }

  @override
  void initState() {
    images.addAll(widget.list);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: images.map((String image) {
        return FadeInImage(
          height: 50,
          width: 50,
          image: NetworkImage(image));
      }).toList(),
    );
  }
}

class MenuCicleBackgroundPainter extends CustomPainter {
  Paint circlePaint;
  Color circleColor;
  double radius;

  MenuCicleBackgroundPainter({this.radius = 100, this.circleColor = Colors.black}) {
    circlePaint = Paint();
    circlePaint.color = circleColor;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(Offset(0, 0), radius, circlePaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
